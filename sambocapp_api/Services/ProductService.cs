﻿using System;
using System.Net.NetworkInformation;
using Microsoft.AspNetCore.Mvc.RazorPages;
using sambocapp_api.Configuration;
using sambocapp_api.Helpers;
using sambocapp_api.Interface;
using sambocapp_api.Models;

namespace sambocapp_api.Services
{
    public class ProductService : IProduct
    {
        private readonly IConfiguration _configuration;
        private string defaultUrlNoImg = "";
        private string basicUrlImg = "", appDomain = "";
        private AppDBContext _contex;
        double pageResult = 10;
        public ProductService(AppDBContext dbConnection, IConfiguration configuration)
        {
            _contex = dbConnection;
            _configuration = configuration;

            appDomain = _configuration.GetSection("Application:AppDomain").Value;
            basicUrlImg = appDomain + ProviderConnector.Image;
            defaultUrlNoImg = appDomain + ProviderConnector.ImageDefaultNoImg;
        }

        public double Count()
        {
            return _contex.products!.Count();
        }

        public double Count(string status)
        {
            return _contex.products!.Where(p => p.Status == status).Count();
        }

        public double Count(int shopid, string status)
        {
            if (status.ToLower() == "all")
            {
                return _contex.products!.Where(p => p.ShopId == shopid).Count();
            }
            return _contex.products!.Where(p => p.ShopId == shopid).Where(s=>s.Status==status).Count();
        }

        public double Count(int shopid)
        {
            return _contex.products!.Where(p => p.ShopId == shopid).Count();
        }

        public double PageCount()
        {
            return Math.Ceiling((double)_contex.products!.Count() / pageResult)!;
        }

        public double PageCount(string status)
        {
            return Math.Ceiling((double)_contex.products!.Where(p => p.Status == status).Count() / pageResult)!;
        }

        public double PageCount(int shopid, string status)
        {
            if (status.ToLower() == "all")
            {
                return Math.Ceiling((double)_contex.products!.Where(p => p.ShopId == shopid).Count() / pageResult)!;
            }
            return Math.Ceiling((double)_contex.products!.Where(p => p.ShopId == shopid).Where(s => s.Status == status).Count() / pageResult)!;
        }

        public List<Product> GetAll()
        {
            var products = _contex.products!
               .OrderByDescending(d => d.id) 
               .ToList();
            if (products != null)
            { 
                return products!;
            }

            return null!;
        }

        public List<Product> GetProduct(int page)
        {
            if (page != 0)
            {
                var location = _contex.products!
                .OrderByDescending(d => d.id)
                .Skip((page - 1) * (int)pageResult)
                .Take((int)pageResult)
                .ToList();
                return location!;
            }

            return null!;
        }


        public List<Product> GetByShopId(int shopid,int page, string status)
        {
            if (page != 0)
            {
                if (status == "all")
                {
                    var all = _contex.products!
                        .Where(s => s.ShopId == shopid)
                        .OrderByDescending(d => d.id)
                        .Skip((page - 1) * (int)pageResult) 
                        .Take((int)pageResult)
                        .ToList();
                    return all!;
                }
                var location = _contex.products!
                    .Where(s => s.ShopId == shopid)
                    .Where(s => s.Status == status)
                    .OrderByDescending(d => d.id)
                    .Skip((page - 1) * (int)pageResult)
                    .Take((int)pageResult) 
                    .ToList();
                return location!;
            }

            return null!;
        }

        public List<Product> GetByShopId(int shopid)
        {
            if (shopid != 0)
            { 
                var location = _contex.products!
                    .Where(s => s.ShopId == shopid) 
                    .Take((int)pageResult)
                    .ToList();
                return location!;
            }

            return null!;
        }

        public Product GetById(int id)
        {
            var product = _contex.products!.Where(l => l.id == id).FirstOrDefault()!;
            if (product != null)
            {
                return product;
            }
            return null!;

        }


        public List<Product> GetByStatus(string status,int page)
        {
            if (page != 0)
            {
                var location = _contex.products!
                    .Where(p => p.Status == status)
                    .OrderByDescending(d => d.id)
                    .Skip((page - 1) * (int)pageResult)
                    .Take((int)pageResult)
                
                    .ToList();
                return location!;
            }

            //List<Product> product = _contex.products!.Where(p =>p.Status==status).ToList();
            //if (product != null)
            //{
            //    return product;
            //}
            return null!;

        }

        public Product CreateNew(Product req)
        {
            _contex.Add(req);
            _contex.SaveChanges();
            Product result = _contex.products!.Where(u => u.id == req.id).FirstOrDefault()!;
            return result;

        }

        public Product Update(Product req)
        {
            Product product = _contex.products!.FirstOrDefault(c => c.id == req.id)!;
            product.ShopId = req.ShopId;
            product.ProductCode = req.ProductCode;
            product.ProductName = req.ProductName;
            product.Description = req.Description;
            product.QtyInStock = req.QtyInStock;
            product.Price = req.Price;
            product.CurrencyId = req.CurrencyId;
            product.CutStockType = req.CutStockType;
            product.ExpiredDate = req.ExpiredDate;
            product.LinkVideo = req.LinkVideo;
            product.ImageThumbnail = req.ImageThumbnail;
            product.Status = req.Status;
            _contex.SaveChanges();
            Product result = _contex.products!.Where(c => c.id == req.id).FirstOrDefault()!;
            if (result != null)
            {
                return result;
            }
            return null!;

        }

    }
}

