﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace sambocapp_api.Controllers
{
    [Route("/app/v1/uploadfile")]
    [ApiController, Authorize]
    public class UploadFileController : ControllerBase
    {
        [HttpPost("singlefile")]
        public IActionResult UploadFile(IFormFile file)
        {
            string fileName = "";
            try
            {
                if (file != null)
                {
                    string uploadsFolder = Path.Combine(Directory.GetCurrentDirectory(), "imagesUpload");
                    string nameFile = Path.Combine(
                             Path.GetDirectoryName(file.FileName)!
                              , string.Concat(
                                 DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss")
                                 , Path.GetExtension(file.FileName)

                             ));
                    fileName = nameFile;
                    string filePath = Path.Combine(uploadsFolder, nameFile);
                    var fileStream = new FileStream(filePath, FileMode.Create);
                     file.CopyTo(fileStream);
                }
            }
            catch (Exception)
            {

            }
            return Ok(new { filename=fileName });
        }
    }
}

