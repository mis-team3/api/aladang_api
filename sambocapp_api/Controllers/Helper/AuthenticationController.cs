using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using sambocapp_api.Helpers;
using sambocapp_api.Models.AppAuthorize;

namespace sambocapp_api.Controllers.Helper
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class AuthenticationController : ControllerBase
    {
        [HttpPost("login")]
        public IActionResult Login([FromBody] AuthenticateRequest user)
        {
            //var encrypt = Encrypt.EncriptSha256PassWord("samboc123");
            //string tt = encrypt;
            //samboc123=811d59a35920570c3b8207bfe09eeeea7d727a34b0b0bdad84d1064c2913fddf

            if (user is null)
            {
                return BadRequest("Invalid user request!!!");
            }
            if (user.Username == "adminApi2023" && user.Password == "Pass@777s;ldjfdjwyew(76676655")
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigurationManagerApp.AppSetting["JWT:Secret"]));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                var tokeOptions = new JwtSecurityToken(issuer: ConfigurationManagerApp.AppSetting["JWT:ValidIssuer"], audience: ConfigurationManagerApp.AppSetting["JWT:ValidAudience"], claims: new List<Claim>(), expires: DateTime.Now.AddDays(6), signingCredentials: signinCredentials);
                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return Ok(new JWTTokenResponse
                {
                    Token = tokenString
                });
            }
            return Unauthorized();
        }
    }
}
