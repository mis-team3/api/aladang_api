﻿using System;
using System.Collections;
using sambocapp_api.Models;
using sambocapp_api.ViewModel;

namespace sambocapp_api.Interface
{
	public interface IReport
	{
        public List<OrderDetailViewModel> GetOrderDetail();
        public List<OrderDetailViewModel> GetOrderDetailByShopId(int id);
    }
}

