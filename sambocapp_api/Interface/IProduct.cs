﻿using System;
using sambocapp_api.Models;

namespace sambocapp_api.Interface
{
    public interface IProduct
    {
        public List<Product> GetAll();
        public List<Product> GetProduct(int page);
        public List<Product> GetByShopId(int shopid, int page, string status);
        public List<Product> GetByShopId(int shopid);
        public List<Product> GetByStatus(string status, int page);

        public double Count();
        public double Count(string status);
        public double Count(int shopid, string status);
        public double Count(int shopid);

        public double PageCount();
        public double PageCount(string status);
        public double PageCount(int shopid, string status);

        

        public Product GetById(int id);
        public Product Update(Product req);
        public Product CreateNew(Product req);
    }
}

